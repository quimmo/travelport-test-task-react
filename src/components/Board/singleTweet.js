import React, { Component } from 'react';

export default class SingleTweet extends Component {


  render() {
    return (
      <div className="tweet-container">
        <div className="tweet-user-name">{this.props.name}</div>
        <div className="tweet-text">{this.props.text}</div>
      </div>
    );
  }
}