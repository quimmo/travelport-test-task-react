import { TWEETS_GET_START, TWEETS_GET_OK, TWEETS_GET_FAIL } from '../actions/boardActions';

const initialState = {
  tweets: [],
  loading: false,
  error: false,
}

export default function boardReducer(state = initialState, action) {
  switch (action.type) {
    case TWEETS_GET_START:
      return {
        ...state,
        loading: true,
      }
    
    case TWEETS_GET_OK: 
      return {
        ...state,
        loading: false,
        tweets: action.payload,
      }
    
    case TWEETS_GET_FAIL:
      return {
        ...state,
        error: true,
      }
    
    default:
      return state;
  }
}