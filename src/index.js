import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import store from './store';
import Board from './components/Board';
import './static/board.css';

ReactDOM.render(
  <Provider store={store}>
    <Board/>
  </Provider>,
  document.getElementById('root'),
);
registerServiceWorker();
