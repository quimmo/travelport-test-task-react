import React, { Component } from 'react';
import Tweet from './singleTweet';

export default class TweetList extends Component {


  render() {
    
    if (this.props.loading) {
      return <div className="loader">Loading...</div>
    }

    if (this.props.error) {
      return <div className="error">ERROR!</div>
    }

    if(this.props.tweets.payload !== undefined) {
      return (
        <ul>
          {this.props.tweets.payload.statuses.map((tweet) => <li><Tweet name={tweet.user.name} text={tweet.text}/></li>)}
        </ul>
      );
    }
    return <div></div>
  }
}