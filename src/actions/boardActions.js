export const TWEETS_GET_START = 'TWEETS_GET_START';
export const TWEETS_GET_OK = 'TWEETS_GET_OK';
export const TWEETS_GET_FAIL = 'TWEETS_GET_FAIL';

export function tweetsGetStart() {
  return { type: TWEETS_GET_START }
}

export function tweetsGetOk(payload) {
  return { type: TWEETS_GET_OK, payload }
}

export function tweetsGetFail() {
  return { type: TWEETS_GET_FAIL }
}

export function getTweets(query) {
  return async (dispatch) => {
    dispatch(tweetsGetStart);

    const server = '';
    const url = `http://${server}/twitter/hashtag?tag=${query}`
    try {
      const result = await fetch(url);
      const data = await result.json();
      setTimeout(
        dispatch(tweetsGetOk(data)),
        2000,
      )
    } catch (error) {
      dispatch(tweetsGetFail());
    }
  }
}