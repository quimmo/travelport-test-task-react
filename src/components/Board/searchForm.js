import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getTweets } from '../../actions/boardActions';


class SearchForm extends Component {


  constructor(props) {
    super(props)
    this.state = {
      searchQuery: '',
    }
  }

  async fetchTweets() {
    this.props.dispatch(getTweets(this.state.searchQuery));
  }

  createQuery(event) {
    this.setState({
      searchQuery: event.target.value,
    })
  }

  render() {
    return (
      <div className="search-form">
        <input type="text" id="tweetsSearchField" value={this.state.searchQuery} onChange={event => this.createQuery(event)}/>
        <input type="button" value="Search" onClick={() => this.fetchTweets()}/>
      </div>
    );
  }
}

export default connect()(SearchForm);