import React, { Component } from 'react';
import { connect } from 'react-redux';
import Tweets from './tweetList';
import SearchForm from './searchForm';

class Board extends Component {
  render() {
    console.log(this.props);
    return (
         <div className="board-wrapper">
           <SearchForm />
           <Tweets tweets={this.props.tweets} loading={this.props.loading} error={this.props.error} />
         </div>
     )
  }
}

const mapStateToProps = state => ({
  tweets: state.board.tweets,
  loading: state.board.loading,
  error: state.board.error,
});

export default connect(mapStateToProps)(Board);